Juggler
=======
A TCP and HTTP load balancer built on top of Netty (Based on the [proxy example](https://github.com/netty/netty/tree/4.1/example/)).

Configuration
=============

HTTP Connections
----------------

The HTTP load balancer supports sticky sessions, currently only through the `JSESSIONID` cookie.

To load balance HTTP connections to port 8080 on the localhost interface across ports 8081 and 8082 on the localhost interface, the 
following configuration would be used:

```
<plugins>
  <plugin>
    <groupId>org.marvelution.maven.plugins</groupId>
    <artifactId>juggler-maven-plugin</artifactId>
    <configuration>
      <jugglers>
          <juggler>
            <type>HTTP</type>
            <stickySessions>true</stickySessions>
            <host>*</host>
            <port>808</port>
            <targets>
              <target>
                <host>localhost</host>
                <port>8081</port>
              </target>
              <target>                
                <host>localhost</host>
                <port>8082</port>
              </target>
            </targets>
          </juggler>
      </jugglers>
    </configuration>
  </plugin>
</plugins>
```

Or you may omit `<host/>` for sensible defaults ("*" for the balancer to bind to all interfaces and "localhost" for the targets):

```
<plugins>
  <plugin>
    <groupId>org.marvelution.maven.plugins</groupId>
    <artifactId>juggler-maven-plugin</artifactId>
    <configuration>
      <jugglers>                                 
          <juggler>
	        <type>HTTP</type>
	        <stickySessions>true</stickySessions>
            <port>8080</port>
            <targets>
              <target>
                <port>8081</port>
              </target>
              <target>
                <port>8082</port>
              </target>
            </targets>
          </juggler>
      </jugglers>
    </configuration>
  </plugin>
</plugins>
```

TCP Connections
----------------

To balance TCP connections simply change the `<type/>` to `TCP` and remove `<stickySessions/>`:

```
<plugins>
  <plugin>
    <groupId>org.marvelution.maven.plugins</groupId>
    <artifactId>juggler-maven-plugin</artifactId>
    <configuration>
      <jugglers>
          <juggler>
            <type>TCP</type>
            <host>*</host>
            <port>808</port>
            <targets>
              <target>
                <host>localhost</host>
                <port>8081</port>
              </target>
              <target>                
                <host>localhost</host>
                <port>8082</port>
              </target>
            </targets>
          </juggler>
      </jugglers>
    </configuration>
  </plugin>
</plugins>
```

Or you may omit `<type/>` and `<host/>` for sensible defaults (TCP for type, "*" for the balancer to bind to all interfaces and "localhost" 
for the targets):

```
<plugins>
  <plugin>
    <groupId>org.marvelution.maven.plugins</groupId>
    <artifactId>juggler-maven-plugin</artifactId>
    <configuration>
      <jugglers>
          <juggler>
            <port>808</port>
            <targets>
              <target>
                <port>8081</port>
              </target>
              <target>                
                <port>8082</port>
              </target>
            </targets>
          </juggler>
      </jugglers>
    </configuration>
  </plugin>
</plugins>
```

Multiple Jugglers
-----------------

Multiple `<juggler/>` elements may be specified to start more than one balancer at a time e.g.
