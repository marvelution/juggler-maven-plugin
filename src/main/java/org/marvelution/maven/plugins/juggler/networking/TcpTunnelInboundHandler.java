/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.networking;

import org.marvelution.maven.plugins.juggler.TcpJuggler;
import org.marvelution.maven.plugins.juggler.configuration.Target;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * TCP tunnel inbound channel handler.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class TcpTunnelInboundHandler extends TunnelInboundHandler<TcpJuggler, ByteBuf> {

	private Channel outboundChannel;

	public TcpTunnelInboundHandler(TcpJuggler juggler) {
		super(juggler);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		Channel inboundChannel = ctx.channel();
		outboundChannel = initializeOutboundChannel(inboundChannel, null).addListener(future -> {
			if (future.isSuccess()) {
				// connection complete start to read first data
				inboundChannel.read();
			} else {
				// Close the connection if the connection attempt has failed.
				inboundChannel.close();
			}
		}).channel();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		if (outboundChannel != null && outboundChannel.isActive()) {
			outboundChannel.writeAndFlush(msg).addListener((ChannelFutureListener) future -> {
				if (future.isSuccess()) {
					// was able to flush out data, start to read the next chunk
					ctx.channel().read();
				} else {
					future.channel().close();
				}
			});
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) { }

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		if (outboundChannel != null) {
			closeOnFlush(outboundChannel);
		}
	}

	@Override
	protected ChannelInitializer<SocketChannel> channelInitializer(Channel inboundChannel, Target target) {
		return new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel channel) {
				channel.pipeline().addLast(new TcpTunnelOutboundHandler(inboundChannel));
			}
		};
	}
}
