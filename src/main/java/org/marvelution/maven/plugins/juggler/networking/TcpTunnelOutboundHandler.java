/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.networking;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;

/**
 * TCP tunnel outbound channel handler.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class TcpTunnelOutboundHandler extends ChannelInboundHandler<ByteBuf> {

	private final Channel inboundChannel;

	TcpTunnelOutboundHandler(Channel inboundChannel) {
		this.inboundChannel = inboundChannel;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		ctx.read();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		inboundChannel.writeAndFlush(msg).addListener((ChannelFutureListener) future -> {
			if (future.isSuccess()) {
				ctx.channel().read();
			} else {
				future.channel().close();
			}
		});
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) { }

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		closeOnFlush(inboundChannel);
	}
}
