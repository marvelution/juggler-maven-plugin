/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.stickiness;

import java.util.Iterator;
import java.util.Optional;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link StickySessionStrategy} that looksup the session id from the {@link #JSESSIONID} cookie.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class JSessionCookieStickySessionStrategy implements StickySessionStrategy {

	private static final Logger LOGGER = LoggerFactory.getLogger(JSessionCookieStickySessionStrategy.class);
	static final String SET_COOKIE = "Set-Cookie";
	static final String COOKIE = "Cookie";
	static final String JSESSIONID = "JSESSIONID";

	@Override
	public Optional<String> determineSessionId(Channel channel, Object msg) {
		if (msg instanceof FullHttpMessage) {
			FullHttpMessage message = (FullHttpMessage) msg;
			String sessionId = locateSessionIdFromCookie(message.headers().valueStringIterator(SET_COOKIE));
			if (sessionId == null) {
				sessionId = locateSessionIdFromCookie(message.headers().valueStringIterator(COOKIE));
			}
			return Optional.ofNullable(sessionId);
		}
		return Optional.empty();
	}

	private String locateSessionIdFromCookie(Iterator<String> cookies) {
		while (cookies.hasNext()) {
			String cookie = cookies.next();
			if (cookie.contains(JSESSIONID)) {
				LOGGER.debug("Located {} cookie: {}", JSESSIONID, cookie);
				int start = cookie.indexOf(JSESSIONID) + JSESSIONID.length() + 1;
				int end = cookie.indexOf(";", start);
				if (end > start) {
					return cookie.substring(start, end);
				} else {
					return cookie.substring(start);
				}
			}
		}
		return null;
	}
}
