/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.configuration;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * {@link Target} that supports being bound to a session.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class StickyTarget extends Target {

	private final Set<String> sessionIds = new CopyOnWriteArraySet<>();

	public StickyTarget(Target target) {
		super(target.getHost(), target.getPort());
	}

	/**
	 * Bind this {@link StickyTarget target} to the specified {@code sessionId}.
	 */
	public void bind(String sessionId) {
		sessionIds.add(sessionId);
	}

	/**
	 * Check if this {@link StickyTarget target} is bound to the specified {@code sessionId}.
	 */
	public boolean isBound(String sessionId) {
		return sessionIds.contains(sessionId);
	}
}
