/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.configuration;

import java.util.Objects;

import org.apache.maven.plugins.annotations.Parameter;

/**
 * Balancing target host.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class Target {

	private static final String LOCALHOST = "localhost";
	@Parameter(defaultValue = LOCALHOST)
	private String host = LOCALHOST;
	@Parameter(required = true)
	private int port;

	public Target() { }

	public Target(int port) {
		this(LOCALHOST, port);
	}

	public Target(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Target)) {
			return false;
		}
		Target target = (Target) o;
		return port == target.port &&
				Objects.equals(host, target.host);
	}

	@Override
	public int hashCode() {
		return Objects.hash(host, port);
	}

	@Override
	public String toString() {
		return host + ":" + port;
	}
}
