/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.configuration;

/**
 * Types of tunneling supported.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public enum JugglerType {

	HTTP(true), TCP(false);

	private final boolean supportsStickySessions;

	JugglerType(boolean supportsStickySessions) {
		this.supportsStickySessions = supportsStickySessions;
	}

	public boolean supportsStickySessions() {
		return supportsStickySessions;
	}
}
