/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler;

import org.marvelution.maven.plugins.juggler.balancing.BalancingStrategy;
import org.marvelution.maven.plugins.juggler.configuration.JugglerConfig;
import org.marvelution.maven.plugins.juggler.networking.HttpTunnelInboundHandler;
import org.marvelution.maven.plugins.juggler.stickiness.StickySessionStrategy;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;

/**
 * {@link Juggler} implementation for HTTP tunnels.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class HttpJuggler extends Juggler {

	private final StickySessionStrategy stickySessionStrategy;

	public HttpJuggler(JugglerConfig config, BalancingStrategy balancingStrategy, StickySessionStrategy stickySessionStrategy) {
		super(config, balancingStrategy);
		this.stickySessionStrategy = stickySessionStrategy;
	}

	@Override
	protected ChannelInitializer<SocketChannel> channelInitializer() {
		return new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel channel) {
				channel.pipeline()
				       .addLast(new HttpServerCodec())
				       .addLast(new HttpObjectAggregator(Integer.MAX_VALUE))
				       .addLast(new HttpTunnelInboundHandler(HttpJuggler.this));
			}
		};
	}

	public StickySessionStrategy stickySessionStrategy() {
		return stickySessionStrategy;
	}
}
