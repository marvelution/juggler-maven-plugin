/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.networking;

import org.marvelution.maven.plugins.juggler.Juggler;
import org.marvelution.maven.plugins.juggler.balancing.BalancingStrategy;
import org.marvelution.maven.plugins.juggler.configuration.Target;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;

/**
 * Base Tunnel Inbound {@link ChannelInboundHandler}.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
abstract class TunnelInboundHandler<J extends Juggler, I> extends ChannelInboundHandler<I> {

	private final BalancingStrategy balancingStrategy;

	TunnelInboundHandler(J juggler) {
		this.balancingStrategy = juggler.balancingStrategy();
	}

	/**
	 * Initializes the outbound channel.
	 */
	synchronized ChannelFuture initializeOutboundChannel(Channel inboundChannel, String sessionId) {
		Target target;
		if (sessionId == null) {
			target = balancingStrategy.selectTarget();
		} else {
			target = balancingStrategy.selectTarget(sessionId);
		}

		// Start the connection attempt.
		return new Bootstrap()
				.channel(inboundChannel.getClass())
				.handler(channelInitializer(inboundChannel, target))
				.option(ChannelOption.AUTO_READ, false)
				.group(eventLoopGroup(inboundChannel))
				.connect(target.getHost(), target.getPort());
	}

	protected abstract ChannelInitializer<SocketChannel> channelInitializer(Channel inboundChannel, Target target);

	protected EventLoopGroup eventLoopGroup(Channel channel) {
		return channel.eventLoop();
	}
}
