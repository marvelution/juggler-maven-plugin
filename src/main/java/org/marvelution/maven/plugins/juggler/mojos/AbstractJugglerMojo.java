/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.mojos;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.marvelution.maven.plugins.juggler.HttpJuggler;
import org.marvelution.maven.plugins.juggler.Juggler;
import org.marvelution.maven.plugins.juggler.TcpJuggler;
import org.marvelution.maven.plugins.juggler.balancing.BalancingStrategy;
import org.marvelution.maven.plugins.juggler.balancing.RoundRobinBalancingStrategy;
import org.marvelution.maven.plugins.juggler.configuration.JugglerConfig;
import org.marvelution.maven.plugins.juggler.configuration.Target;
import org.marvelution.maven.plugins.juggler.stickiness.JSessionCookieStickySessionStrategy;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import static org.marvelution.maven.plugins.juggler.configuration.JugglerConfig.ANY_HOST;

/**
 * Base Juggler Mojo.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
abstract class AbstractJugglerMojo extends AbstractMojo {

	private static final List<Juggler> JUGGLERS = new LinkedList<>();
	@Parameter(alias = "jugglers")
	private List<JugglerConfig> jugglerConfigs;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		doExecute();
	}

	protected abstract void doExecute() throws MojoExecutionException, MojoFailureException;

	void startJugglers() throws MojoExecutionException, MojoFailureException {
		validateConfig();
		for (JugglerConfig config : jugglerConfigs) {
			try {
				getLog().info(String.format("Starting juggler at %s", config));
				BalancingStrategy balancingStrategy = new RoundRobinBalancingStrategy(config.getTargets());
				Juggler juggler;
				switch (config.getType()) {
					case HTTP:
						juggler = new HttpJuggler(config, balancingStrategy, new JSessionCookieStickySessionStrategy());
						break;
					case TCP:
						juggler = new TcpJuggler(config, balancingStrategy);
						break;
					default:
						throw new MojoFailureException("Unsupported juggler type " + config.getType());
				}
				if (!juggler.start()) {
					throw new MojoExecutionException(String.format("Failed to start juggler at %s", config));
				}
				JUGGLERS.add(juggler);
			} catch (MojoExecutionException e) {
				throw e;
			} catch (Exception e2) {
				throw new MojoExecutionException(String.format("Failed to start juggler at %s", config), e2);
			}
		}
	}

	private void validateConfig() throws MojoFailureException {
		for (JugglerConfig config : jugglerConfigs) {
			if (!config.getType().supportsStickySessions() && config.isStickySessions()) {
				throw new MojoFailureException("Juggler tpe " + config.getType() + " does not support sticky sessions.");
			}
			if (config.getTargets().length == 0) {
				throw new MojoFailureException("Jugglers must have at least one target");
			}
			for (final Target target : config.getTargets()) {
				if (Objects.equals(ANY_HOST, target.getHost())) {
					throw new MojoFailureException("Juggler target hosts may not be '*'");
				}
			}
		}
	}

	void stopJugglers() {
		for (Juggler juggler : JUGGLERS) {
			juggler.stop();
		}
		JUGGLERS.clear();
	}
}
