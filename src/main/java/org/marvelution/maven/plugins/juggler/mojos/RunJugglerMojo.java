/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.mojos;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Mojo to run jugglers.
 *
 * @author Mark Rekveld
 */
@Mojo(name = "run")
public class RunJugglerMojo extends AbstractJugglerMojo {

	@Override
	protected void doExecute() throws MojoExecutionException, MojoFailureException {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			RunJugglerMojo.this.getLog().info("Running Shutdown Hook");
			try {
				RunJugglerMojo.this.stopJugglers();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to shut down products in shutdown hook");
			}
		}));
		startJugglers();
		getLog().info("Type Ctrl-C to shutdown gracefully");
		try {
			int r;
			while ((r = System.in.read()) != -1) {
			}
		} catch (Exception ignored) {
		}
		stopJugglers();
	}
}
