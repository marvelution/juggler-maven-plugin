/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.configuration;

import org.marvelution.maven.plugins.juggler.Juggler;

import org.apache.maven.plugins.annotations.Parameter;

/**
 * Configuration for a single {@link Juggler}.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class JugglerConfig {

	public static final String ANY_HOST = "*";
	@Parameter(required = true, defaultValue = "TCP")
	private JugglerType type = JugglerType.TCP;
	private boolean stickySessions;
	@Parameter(defaultValue = ANY_HOST)
	private String host = ANY_HOST;
	@Parameter(required = true)
	private int port;
	@Parameter(alias = "target", required = true)
	private Target[] targets;

	public JugglerType getType() {
		return type;
	}

	public JugglerConfig setType(JugglerType type) {
		this.type = type;
		return this;
	}

	public boolean isStickySessions() {
		return stickySessions;
	}

	public JugglerConfig setStickySessions(boolean stickySessions) {
		this.stickySessions = stickySessions;
		return this;
	}

	public String getHost() {
		return host;
	}

	public JugglerConfig setHost(String host) {
		this.host = host;
		return this;
	}

	public int getPort() {
		return port;
	}

	public JugglerConfig setPort(int port) {
		this.port = port;
		return this;
	}

	public Target[] getTargets() {
		return targets;
	}

	public JugglerConfig setTargets(Target... targets) {
		this.targets = targets;
		return this;
	}

	@Override
	public String toString() {
		return host + ":" + port;
	}
}
