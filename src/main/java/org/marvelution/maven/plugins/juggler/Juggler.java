/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler;

import java.util.Objects;

import org.marvelution.maven.plugins.juggler.balancing.BalancingStrategy;
import org.marvelution.maven.plugins.juggler.configuration.JugglerConfig;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import static org.marvelution.maven.plugins.juggler.configuration.JugglerConfig.ANY_HOST;

/**
 * Base Juggler implementation.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public abstract class Juggler {

	private final JugglerConfig config;
	private final BalancingStrategy balancingStrategy;
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private Channel serverChannel;

	Juggler(JugglerConfig config, BalancingStrategy balancingStrategy) {
		this.config = config;
		this.balancingStrategy = balancingStrategy;
	}

	/**
	 * Starts the new Juggler.
	 */
	public boolean start() {
		bossGroup = new NioEventLoopGroup();
		workerGroup = new NioEventLoopGroup();

		try {
			ServerBootstrap bootstrap = new ServerBootstrap()
					.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(channelInitializer())
					.childOption(ChannelOption.AUTO_READ, false);
			ChannelFuture bind;
			if (Objects.equals(ANY_HOST, config.getHost())) {
				bind = bootstrap.bind(config.getPort());
			} else {
				bind = bootstrap.bind(config.getHost(), config.getPort());
			}
			serverChannel = bind.sync().channel();
			return serverChannel.isActive();
		} catch (Exception e) {
			e.printStackTrace();
			serverChannel.close();
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
			return false;
		}
	}

	protected abstract ChannelInitializer<SocketChannel> channelInitializer();

	/**
	 * Stops the juggler.
	 */
	public void stop() {
		serverChannel.close();
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
	}

	public JugglerConfig config() {
		return config;
	}

	public BalancingStrategy balancingStrategy() {
		return balancingStrategy;
	}
}
