/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.networking;

import org.marvelution.maven.plugins.juggler.configuration.StickyTarget;
import org.marvelution.maven.plugins.juggler.configuration.Target;
import org.marvelution.maven.plugins.juggler.stickiness.StickySessionStrategy;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpMessage;

import static io.netty.channel.ChannelFutureListener.CLOSE;

/**
 * HTTP tunnel outbound channel handler.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class HttpTunnelOutboundHandler extends ChannelInboundHandler<FullHttpMessage> {

	private final Channel inboundChannel;
	private final Target target;
	private final StickySessionStrategy stickySessionStrategy;

	HttpTunnelOutboundHandler(Channel inboundChannel, Target target, StickySessionStrategy stickySessionStrategy) {
		this.inboundChannel = inboundChannel;
		this.target = target;
		this.stickySessionStrategy = stickySessionStrategy;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		ctx.read();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpMessage msg) {
		if (target instanceof StickyTarget && stickySessionStrategy != null) {
			stickySessionStrategy.determineSessionId(ctx.channel(), msg).ifPresent(((StickyTarget) target)::bind);
		}
		inboundChannel.writeAndFlush(msg.retain()).addListener(CLOSE);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		closeOnFlush(inboundChannel);
	}
}
