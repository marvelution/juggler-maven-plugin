/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.balancing;

import java.util.List;

import org.marvelution.maven.plugins.juggler.configuration.Target;

/**
 * Juggler Balancing Strategy.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public interface BalancingStrategy<T extends Target> {

	/**
	 * Select the next {@link T target}.
	 */
	T selectTarget();

	/**
	 * Select the next {@link T target} taking the specified {@code sessionId} into account.
	 */
	T selectTarget(String sessionId);

	/**
	 * Return all the available targets.
	 */
	List<T> geTargets();
}
