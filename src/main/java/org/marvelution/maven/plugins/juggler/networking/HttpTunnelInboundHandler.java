/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.networking;

import org.marvelution.maven.plugins.juggler.HttpJuggler;
import org.marvelution.maven.plugins.juggler.configuration.Target;
import org.marvelution.maven.plugins.juggler.stickiness.StickySessionStrategy;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.FullHttpMessage;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;

import static java.lang.Integer.MAX_VALUE;

/**
 * HTTP tunnel inbound channel handler.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class HttpTunnelInboundHandler extends TunnelInboundHandler<HttpJuggler, FullHttpMessage> {

	private final StickySessionStrategy stickySessionStrategy;
	private final boolean stickyTarget;
	private Channel outboundChannel;

	public HttpTunnelInboundHandler(HttpJuggler juggler) {
		super(juggler);
		stickySessionStrategy = juggler.stickySessionStrategy();
		stickyTarget = juggler.config().isStickySessions();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		if (stickyTarget) {
			// Start reading and initialize the outbound channel based on the first message
			ctx.read();
		} else {
			initializeOutboundChannel(ctx.channel(), null, false);
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FullHttpMessage msg) {
		if (stickyTarget) {
			String sessionId = stickySessionStrategy.determineSessionId(ctx.channel(), msg).orElse(null);
			initializeOutboundChannel(ctx.channel(), sessionId, true);
		}
		if (outboundChannel != null && outboundChannel.isActive()) {
			outboundChannel.writeAndFlush(msg.retain());
		}
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		if (outboundChannel != null) {
			closeOnFlush(outboundChannel);
		}
	}

	@Override
	protected ChannelInitializer<SocketChannel> channelInitializer(Channel inboundChannel, Target target) {
		return new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel channel) {
				channel.pipeline()
				       .addLast(new HttpClientCodec())
				       .addLast(new HttpObjectAggregator(MAX_VALUE))
				       .addLast(new HttpTunnelOutboundHandler(inboundChannel, target, stickyTarget ? stickySessionStrategy : null));
			}
		};
	}

	@Override
	protected EventLoopGroup eventLoopGroup(Channel channel) {
		return channel.parent().eventLoop();
	}

	private synchronized void initializeOutboundChannel(Channel inboundChannel, String sessionId, boolean sync) {
		if (outboundChannel == null) {
			ChannelFuture channelFuture = initializeOutboundChannel(inboundChannel, sessionId).addListener(future -> {
				if (future.isSuccess()) {
					if (!stickyTarget) {
						// connection complete start to read first data
						inboundChannel.read();
					}
				} else {
					// Close the connection if the connection attempt has failed.
					inboundChannel.close();
				}
			});
			if (sync) {
				outboundChannel = channelFuture.syncUninterruptibly().channel();
			} else {
				outboundChannel = channelFuture.channel();
			}
		}
	}
}
