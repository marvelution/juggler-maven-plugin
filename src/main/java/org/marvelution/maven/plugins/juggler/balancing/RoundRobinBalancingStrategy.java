/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.balancing;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.marvelution.maven.plugins.juggler.configuration.StickyTarget;
import org.marvelution.maven.plugins.juggler.configuration.Target;

import static java.util.stream.Collectors.toList;

/**
 * Bound Robin implementation of {@link BalancingStrategy} that also supports {@link StickyTarget sticky targets}.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
public class RoundRobinBalancingStrategy implements BalancingStrategy<StickyTarget> {

	private final List<StickyTarget> targets;
	private final AtomicInteger lastSelected;

	public RoundRobinBalancingStrategy(Target... targets) {
		if (targets == null || targets.length == 0) {
			throw new IllegalArgumentException("targets cannot be empty");
		}
		this.targets = Stream.of(targets).map(StickyTarget::new).collect(toList());
		lastSelected = new AtomicInteger();
	}

	@Override
	public StickyTarget selectTarget() {
		int selected;
		synchronized (lastSelected) {
			selected = lastSelected.getAndIncrement();
			if (selected >= targets.size()) {
				selected = 0;
				lastSelected.set(0);
			}
		}
		return targets.get(selected);
	}

	@Override
	public StickyTarget selectTarget(String sessionId) {
		return targets.stream().filter(target -> target.isBound(sessionId)).findFirst().orElseGet(this::selectTarget);
	}

	@Override
	public List<StickyTarget> geTargets() {
		return Collections.unmodifiableList(targets);
	}
}
