/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.stickiness;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Arrays;
import java.util.Optional;

import org.marvelution.testing.TestSupport;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.FullHttpMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;

import static org.marvelution.maven.plugins.juggler.stickiness.JSessionCookieStickySessionStrategy.COOKIE;
import static org.marvelution.maven.plugins.juggler.stickiness.JSessionCookieStickySessionStrategy.JSESSIONID;
import static org.marvelution.maven.plugins.juggler.stickiness.JSessionCookieStickySessionStrategy.SET_COOKIE;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link JSessionCookieStickySessionStrategy}.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
class JSessionCookieStickySessionStrategyTest extends TestSupport {

	@Mock
	private Channel channel;
	@Mock
	private FullHttpMessage fullHttpMessage;
	private JSessionCookieStickySessionStrategy stickySessionStrategy;

	@BeforeEach
	void setUp() {
		stickySessionStrategy = new JSessionCookieStickySessionStrategy();
	}

	@Test
	void testDetermineSessionId_NotAFullHttpMessage() {
		assertThat(stickySessionStrategy.determineSessionId(channel, new Object()).isPresent(), is(false));
	}

	@Test
	void testDetermineSessionId_EmptyHeaders() {
		when(fullHttpMessage.headers()).thenReturn(new DefaultHttpHeaders(false));
		assertThat(stickySessionStrategy.determineSessionId(channel, fullHttpMessage).isPresent(), is(false));
	}

	@Target(METHOD)
	@Retention(RUNTIME)
	@Documented
	@ParameterizedTest(name = "{0}")
	@ValueSource(strings = { SET_COOKIE, COOKIE })
	@interface CookieHeaderTest {
	}

	@CookieHeaderTest
	void testDetermineSessionId_NoSessionCookie(String cookieHeader) {
		DefaultHttpHeaders headers = new DefaultHttpHeaders(false);
		headers.add(cookieHeader, "myCookie=my cookie value");
		when(fullHttpMessage.headers()).thenReturn(headers);
		assertThat(stickySessionStrategy.determineSessionId(channel, fullHttpMessage).isPresent(), is(false));
	}

	@CookieHeaderTest
	void testDetermineSessionId_SessionCookie(String cookieHeader) {
		DefaultHttpHeaders headers = new DefaultHttpHeaders(false);
		headers.add(cookieHeader, JSESSIONID + "=my-session-id");
		when(fullHttpMessage.headers()).thenReturn(headers);

		Optional<String> sessionId = stickySessionStrategy.determineSessionId(channel, fullHttpMessage);
		assertThat(sessionId.isPresent(), is(true));
		assertThat(sessionId.get(), is("my-session-id"));
	}

	@CookieHeaderTest
	void testDetermineSessionId_MultipleCookies(String cookieHeader) {
		DefaultHttpHeaders headers = new DefaultHttpHeaders(false);
		headers.add(cookieHeader, Arrays.asList(JSESSIONID + "=my-session-id", "myCookie=my cookie value"));
		when(fullHttpMessage.headers()).thenReturn(headers);

		Optional<String> sessionId = stickySessionStrategy.determineSessionId(channel, fullHttpMessage);
		assertThat(sessionId.isPresent(), is(true));
		assertThat(sessionId.get(), is("my-session-id"));
	}

	@CookieHeaderTest
	void testDetermineSessionId_MultipleCookiesOneHeader(String cookieHeader) {
		DefaultHttpHeaders headers = new DefaultHttpHeaders(false);
		headers.add(cookieHeader, JSESSIONID + "=my-session-id; myCookie=my cookie value");
		when(fullHttpMessage.headers()).thenReturn(headers);

		Optional<String> sessionId = stickySessionStrategy.determineSessionId(channel, fullHttpMessage);
		assertThat(sessionId.isPresent(), is(true));
		assertThat(sessionId.get(), is("my-session-id"));
	}

	@CookieHeaderTest
	void testDetermineSessionId_MultipleCookiesOneHeaderReverse(String cookieHeader) {
		DefaultHttpHeaders headers = new DefaultHttpHeaders(false);
		headers.add(cookieHeader, "myCookie=my cookie value; " + JSESSIONID + "=my-session-id");
		when(fullHttpMessage.headers()).thenReturn(headers);

		Optional<String> sessionId = stickySessionStrategy.determineSessionId(channel, fullHttpMessage);
		assertThat(sessionId.isPresent(), is(true));
		assertThat(sessionId.get(), is("my-session-id"));
	}
}
