/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.maven.plugins.juggler.balancing;

import org.marvelution.maven.plugins.juggler.configuration.StickyTarget;
import org.marvelution.maven.plugins.juggler.configuration.Target;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Tests for {@link RoundRobinBalancingStrategy}.
 *
 * @author Mark Rekveld
 * @since 1.0
 */
class RoundRobinBalancingStrategyTest extends TestSupport {

	private RoundRobinBalancingStrategy balancingStrategy;

	@BeforeEach
	void setUp() {
		balancingStrategy = new RoundRobinBalancingStrategy(new Target(8082), new Target(8081));
	}

	@Test
	void testSelectTarget() {
		assertThat(balancingStrategy.selectTarget(), notNullValue());
	}

	@Test
	void testSelectTarget_ResetLastSelectedIfOutOfBounds() {
		assertThat(balancingStrategy.selectTarget(), notNullValue());
		assertThat(balancingStrategy.selectTarget(), notNullValue());
		assertThat(balancingStrategy.selectTarget(), notNullValue());
		assertThat(balancingStrategy.selectTarget(), notNullValue());
		assertThat(balancingStrategy.selectTarget(), notNullValue());
		assertThat(balancingStrategy.selectTarget(), notNullValue());
	}

	@Test
	void testSelectTarget_SessionId() {
		StickyTarget target = balancingStrategy.selectTarget();
		target.bind("session-1");

		assertThat(balancingStrategy.selectTarget("session-1"), is(target));
		assertThat(balancingStrategy.selectTarget("session-2"), not(is(target)));
		assertThat(balancingStrategy.selectTarget("session-1"), is(target));
	}
}
